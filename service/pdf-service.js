const PDFDocument = require('pdfkit');

function buildPDF(dataCallback, endCallback, paper) {
  console.log(paper)
  const doc = new PDFDocument({ bufferPages: true, font: 'Courier', size: 'letter', layout: 'landscape' });

  doc.on('data', dataCallback);
  doc.on('end', endCallback);

  doc.fontSize(20).text('CONGRATULATIONS\n', { align: 'center' });
  doc.fontSize(20).text('This card has been successfully certified.', { align: 'center' });
  doc.fontSize(20).text("\n\n\n\n\n\n\n\n\n\nPaper Title :"+paper.papertitle + "\nName : "+paper.author[0]+"\n", { align: 'center' });
  
  // const text = `\nPaper Title :`+paper.papertitle + "\nName : "+paper.author[0]+"\n";
  // const textWidth = doc.widthOfString(text);
  // const textHeight = doc.currentLineHeight();
  // const x = (doc.page.width - textWidth) / 2;
  // const y = (doc.page.height - textHeight) / 2;


  // doc
  //   .fontSize(17)
  //   .text(text, x, y, { align: 'center' }); // Center the text
  doc.end();
}

module.exports = { buildPDF };