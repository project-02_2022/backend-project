const express = require("express");
const { model } = require("mongoose");
const router = express.Router();
const Paper = require("../model/Paper");
const Conference = require("../model/Conference");
const Category = require("../model/Category");

//Add 1 paper
const addPaper = async (req, res, next) => {
  let paperidnn = 0;
  const papers = await Paper.find({});
  for (let i = 0; i < papers.length; i++) {
    const ramdompaperid = betweenRandomNumber(100000, 999999);
    if (ramdompaperid != papers[i].id) {
      paperidnn = ramdompaperid;
    } else {
      console.log("Paperid Is Duplicate... :(");
      continue;
    }
    // console.log("66 Digit: " + ramdompaperid);
  }

  try {
    const newPaper = new Paper({
      paperid: paperidnn,
      conference: req.body.conference,
      papertitle: req.body.papertitle,
      sessions: req.body.sessions,
      paperstatus: "disapproval",
      detail: req.body.detail,
      author: req.body.author,
      category: req.body.categorys,
    });
    // console.log("author = " + req.body.author)
    await newPaper.save();

    const catarray = req.body.categorys

    for (let i = 0; i < catarray.length; i++) {
      const category = await Category.findOne({
        categoryid: catarray[i]
      })
      const catpaper = category.papers
      catpaper.push(paperidnn)
      category.papers = catpaper
      // console.log(category.papers)
      await category.save();
    }
    res.status(201).json(newPaper);
  } catch (err) {
    return res.status(500).send({
      message: err.message,
    });
  }
};

//Get All paper
const getPapers = async (req, res, next) => {
  try {
    const papers = await Paper.find({});
    res.status(200).json(papers);
  } catch (err) {
    console.log("Error message : " + err);
    return res.status(500).send({
      message: err.message,
    });
  }
};

const pashAuthor = async (req, res, next) => {
  try {
    const paper = await Paper.findOne({ paperid: req.body.dataid })
    // const pushauthor = req.body.data
    // const pushauthor = "author1.3"
    paper.author.push(req.body.datauser)
    await paper.save();
    console.log(req.body.dataid)
    res.status(200).send()
  } catch (err) {
    res.status(404
    ).send(err)
  }
};

//Get 1 paper
const getPaper = async (req, res, next) => {
  const id = req.params.id;
  try {
    const paper = await Paper.find({ paperid: id }).exec();
    // console.log("one paper = " + paper.paperid);
    if (paper === null) {
      return res.status(404).send({
        message: "Paper not found",
      });
    }
    res.json(paper);
  } catch (err) {
    return res.status(404).send({
      message: err.message,
    });
  }
};

//Delete 1 paper
const deletePaper = async function (req, res, next) {
  const id = req.params.id
  try {
    const paper = await Paper.findOneAndDelete({ paperid: id })
    const allpaper = await Paper.find()
    // const paper = await Paper.findOne({ paperid: paperid });
    // const idconferenceinpaper = paper.conference
    // const conferences = await Conference.findOne({ conferenceid: idconferenceinpaper });
    // const arraypapers = conferences.papers
    // for (let i = 0; i < arraypapers.length; i++) {
    //   if (arraypapers[i] == paper.paperid) {
    //     arraypapers.splice(i, 1);
    //     break;
    //   }
    // }
    // const conference = await Conference.findOneAndUpdate({ conferenceid: idconferenceinpaper },
    // )
    // conference.papers = arraypapers
    // await conference.save();
    // await Paper.findOneAndDelete({ paperid: paper.paperid });
    // console.log(arraypapers)
    return res.status(200).send(allpaper);
  } catch (err) {
    return res.status(404).send({ message: err.message });
  }
};

//Get paper not have conference
const getPapersNew = async (req, res, next) => {
  try {
    const papersn = await Paper.find({ conference: 0 });
    res.status(200).json(papersn);
  } catch (err) {
    console.log("Error message : " + err);
    return res.status(500).send({
      message: err.message,
    });
  }
}
//Update 1 paper
const updatePaper = async function (req, res, next) {
  const paperid = req.params.id
  try {
    const paper = await Paper.findOneAndUpdate({ paperid: paperid },
      {
        $set: {
          papertitle: req.body.papertitle, sessions: req.body.sessions, paperstatus: req.body.paperstatus, detail: req.body.detail, category: req.body.category
        }
      });
    await paper.save()


    //update category in paper to category
    const categorypaper = req.body.category
    for (var i = 0; i < categorypaper.length; i++) {
      const category = await Category.findOne({ categoryid: categorypaper[i] });
      category.papers.push(paper.paperid)
      await category.save()
    }

    //check repeat in category
    const allcategory = await Category.find({});
    allcategory.forEach(async (category) => {
      const uniquePapers = [...new Set(category.papers)];
      if (uniquePapers.length !== category.papers.length) {
        category.papers = uniquePapers;
        await category.save();
        // console.log(`Category ${category.categoryname} had duplicates and they were removed.`);
      }
    });
    await allcategory.save()
  
    //delete not found category in paper

    console.log(paper)
    res.status(200).json(paper)
  } catch (err) {
    return res.status(404).send({ message: err.message });
  }
}

function betweenRandomNumber(min, max) {
  return Math.floor(Math.random() * (max - min + 1) + min);
}

router.post("/", addPaper);
router.get("/", getPapers);
router.get("/:id", getPaper);
router.delete("/:id", deletePaper);
router.get("/allnew/:id", getPapersNew);
router.put("/:id", updatePaper);
router.post("/pushauthor/:id", pashAuthor);

module.exports = router;
