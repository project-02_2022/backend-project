const express = require("express");
const { model } = require("mongoose");
const Category = require("../model/Category");
const router = express.Router();
const Categorys = require("../model/Category");
const Conference = require("../model/Conference");
const Paper = require("../model/Paper");

//add category
const addCategory = async (req, res, next) => {
  let categoryid = 0;
  const category = await Category.find({});
  for (let i = 0; i < category.length; i++) {
    const ramdomcategoryid = betweenRandomNumber(10000, 99999);
    if (ramdomcategoryid != category[i].id) {
      categoryid = ramdomcategoryid;
    } else {
      console.log("Categoryid Is Duplicate... :(");
      continue;
    }
    console.log("66 Digit: " + ramdomcategoryid);
  }
  try {
    const newCategory = new Category({
      categoryid: categoryid,
      categoryname: req.body.categoryname,
      categorydescription: req.body.categorydescription,
      papers: [],
    });
    console.log("NewObjectCategoryData = " + newCategory);
    await newCategory.save();
    res.status(201).json(newCategory);
  } catch (err) {
    return res.status(500).send({
      message: err.message,
    });
  }
};

//Get All Category
const getCategorys = async (req, res, next) => {
  try {
    const category = await Categorys.find({});
    res.status(200).json(category);
    // console.log(category);
  } catch (err) {
    console.log("Error message : " + err);
    return res.status(500).send({
      message: err.message,
    });
  }
};
//Get 1 Category
const getCategory = async (req, res, next) => {
  const id = req.params.id;
  try {
    const category = await Category.find({ categoryid: id }).exec();
    console.log("one category = " + category.categoryid);

    if (category === null) {
      return res.status(404).send({
        message: "Category not found",
      });
    }
    res.json(category);
  } catch (err) {
    return res.status(404).send({
      message: err.message,
    });
  }
};

//Get graph 1 conference
const getPaperinCategory = async (req, res, next) => {
  const id = req.params.id;
  try {
    const conference = await Conference.find({ conferenceid: id }).exec();
    const paper = await Paper.find({ conference: conference[0].conferenceid }).exec();
    const arraycate = []
    const arraycaten = []
    const arrayindex = []
    for (let i = 0; i < paper.length; i++) {
      // console.log("num papers = " + [i])
      // console.log("num papers = " + paper[i])
      for (let j = 0; j < paper[i].category.length; j++) {
        // console.log("num cate = " + [j])
        // console.log("name cate = " + paper[i].category[j])
        arraycate.push(paper[i].category[j])
      }
    }

    for (let i = 0; i < arraycate.length; i++) {
      if (arraycaten.indexOf(arraycate[i]) < 0) {
        arraycaten.push(arraycate[i]);
      }

    }

    for (let i = 0; i < arraycaten.length; i++) {
      const category = await Category.find({ categoryid: arraycaten[i] }).exec();
      arrayindex.push(category)
      console.log(category)
    }
    // console.log("Array first = " + arraycate);
    // console.log("Array not null = " + arraycaten);
    // console.log(paper[0].category[1])
    res.status(200).json(arrayindex);
  } catch (err) {
    return res.status(500).send({
      message: err.message,
    });
  }

};

function betweenRandomNumber(min, max) {
  return Math.floor(Math.random() * (max - min + 1) + min);
}

router.get("/", getCategorys);
router.post("/", addCategory);
router.get("/:id", getCategory);
router.get("/graph/:id", getPaperinCategory);

module.exports = router;