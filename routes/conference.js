const express = require("express");
const { model } = require("mongoose");
const router = express.Router();
const Conference = require("../model/Conference");
const Paper = require("../model/Paper");
const ExcelJS = require('exceljs');
const PDFGenerator = require('pdfkit')
const fs = require('fs')
const pdfService = require('../service/pdf-service');
//Add 1 conference
const addConference = async (req, res, next) => {
  const ramdomconferenceid = betweenRandomNumber(100, 999);
  let conferenceidnn = 0;
  const conferences = await Conference.find({});
  for (let i = 0; i < conferences.length; i++) {
    // console.log(papers[i].paperid);
    if (ramdomconferenceid != conferences[i].id) {
      conferenceidnn = ramdomconferenceid;
    } else {
      console.log("Conferenceid Is Duplicate... :(");
    }
  }
  console.log("66 Digit: " + ramdomconferenceid);
  const newConference = new Conference({
    conferenceid: ramdomconferenceid,
    conferencename: req.body.conferencename,
    abbreviation: req.body.abbreviation,
    startdate: req.body.startdate,
    enddate: req.body.enddate,
    venus: req.body.venus,
    type: req.body.type,
    organizer: req.body.organizer,
  });
  console.log("input Conference = " + newConference);
  try {
    await newConference.save();
    res.status(201).json(newConference);
  } catch (err) {
    return res.status(500).send({
      message: err.message,
    });
  }
};

//Get 1 conference
const getConferece = async (req, res, next) => {
  const id = req.params.id;
  try {
    const conference = await Conference.find({ conferenceid: id }).exec();
    // console.log("one conference = " + conference[0].papers);
    if (conference === null) {
      return res.status(404).send({
        message: "Conference not found",
      });
    }
    res.json(conference);
  } catch (err) {
    return res.status(404).send({
      message: err.message,
    });
  }
};

//Get All conference
const getConfereces = async (req, res, next) => {
  try {
    const conferences = await Conference.find({});
    res.status(200).json(conferences);
  } catch (err) {
    console.log("Error message : " + err);
    return res.status(500).send({
      message: err.message,
    });
  }
};

//Get Paper in Conference
const getPaperConfereces = async (req, res, next) => {
  try {
    const papers = await Paper.find({ conference: req.params.id });
    res.status(200).json(papers);
  } catch (err) {
    console.log("Error message : " + err);
    return res.status(500).send({
      message: err.message,
    });
  }
};

//Delete 1 conference
const deleteConference = async function (req, res, next) {
  const conferenceid = req.params.id;
  console.log(conferenceid)
  try {
    const conferencelenght = await Conference.findOne({ conferenceid: conferenceid }).exec();
    const conferlenght = conferencelenght.papers.length
    await Conference.findOneAndDelete({ conferenceid: conferenceid });
    for (let i = 0; i < conferlenght; i++) {
      const paper = await Paper.findOneAndUpdate({ conference: conferenceid }, {

      });
      await paper.save();
    }
    // console.log(paper)
    return res.status(200).json(conferlenght)
  } catch (err) {
    return res.status(404).send({ message: err.message });
  }
};

//Update 1 conference
const updateConference = async function (req, res, next) {
  const conferenceid = req.params.id
  try {
    // const conference = await Conference.findOne({ conferenceid: conferenceid });
    // conference.conferencename = req.body.conferencename
    // conference.abbreviation = req.body.abbreviation
    // conference.datestart = req.body.datestart
    // conference.dateend = req.body.dateend
    // conference.vence = req.body.vence
    // conference.type = req.body.type
    // conference.operate = req.body.operate

    const conference = await Conference.findOneAndUpdate({ conferenceid: 111 }
      , {
        $set: {
          conferencename: req.body.conferencename, abbreviation: req.body.abbreviation,
          startdate: req.body.startdate, enddate: req.body.enddate, venus: req.body.venus, type: req.body.type,
          organizer: req.body.organizer
        }
      });
    await conference.save()
    res.status(200).json(await Conference.findOne({ conferenceid: conferenceid }))
  } catch (err) {
    return res.status(404).send({ message: err.message });
  }
}

//Export 1 Conference and Paper in conference
const exportExcel = async function (req, res, next) {
  try {
    const id = req.params.id;

    // Retrieve the conference data from the database
    const conference = await Conference.find({ conferenceid: id }).exec();
    const papers = await Paper.find({ conference: id }).exec();
    console.log(papers);
    if (!conference || conference.length === 0) {
      return res.status(404).send('Conference not found');
    }

    // Define the data to export
    const data = conference.map(c => ({
      "conferenceid": c.conferenceid,
      "conferencename": c.conferencename,
      "abbreviation": c.abbreviation,
      "startdate": c.startdate,
      "enddate": c.enddate,
      "venus": c.venus,
      "type": c.type,
      "organizer": c.organizer,
      "papers": c.papers,
    }));

    // Create a new workbook
    const wb = new ExcelJS.Workbook();

    // Add a worksheet
    const ws = wb.addWorksheet('Conference');

    // Define the columns for the worksheet
    const conferenceColumns = [
      { header: 'conferenceid', key: 'conferenceid', width: 15 },
      { header: 'conferencename', key: 'conferencename', width: 40 },
      { header: 'abbreviation', key: 'abbreviation', width: 15 },
      { header: 'startdate', key: 'startdate', width: 10 },
      { header: 'enddate', key: 'enddate', width: 10 },
      { header: 'venus', key: 'venus', width: 10 },
      { header: 'type', key: 'type', width: 10 },
      { header: 'organizer', key: 'organizer', width: 10 },
      { header: 'papers', key: 'papers', width: 40 },
    ];

    // Add the column headers to the conference worksheet
    ws.columns = conferenceColumns;

    // Set the header style for the conference worksheet
    ws.getRow(1).fill = {
      type: 'pattern',
      pattern: 'solid',
      fgColor: { argb: 'C5E0B3' }
    };
    ws.getRow(1).font = {
      bold: true
    };

    // Add the conference data to the conference worksheet
    conference.forEach(item => {
      ws.addRow(item);
    });

    // Add a new worksheet for papers
    const paperWorksheet = wb.addWorksheet('Papers');

    // Define the column headers and widths for the paper worksheet
    const paperColumns = [
      { header: 'paperid', key: 'paperid', width: 15 },
      { header: 'conference', key: 'conference', width: 15 },
      { header: 'papertitle', key: 'papertitle', width: 30 },
      { header: 'sessions', key: 'sessions', width: 30 },
      { header: 'paperstatus', key: 'paperstatus', width: 15 },
      { header: 'detail', key: 'detail', width: 40 },
      { header: 'author', key: 'author', width: 40 },
      { header: 'category', key: 'category', width: 40 },
    ];

    // Add the column headers to the paper worksheet
    paperWorksheet.columns = paperColumns;

    // Set the header style for the paper worksheet
    paperWorksheet.getRow(1).fill = {
      type: 'pattern',
      pattern: 'solid',
      fgColor: { argb: 'C5E0B3' }
    };
    paperWorksheet.getRow(1).font = {
      bold: true
    };

    // Add the paper data to the paper worksheet
    papers.forEach(item => {
      paperWorksheet.addRow({
        paperid: item.paperid,
        conference: item.conference,
        papertitle: item.papertitle,
        sessions: item.sessions,
        paperstatus: item.paperstatus,
        detail: item.detail,
        author: item.author,
        category: item.category,
      });
    });

    // Set the response headers and send the workbook as a response
    res.setHeader('Content-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    res.setHeader('Content-Disposition', `attachment; filename=conference_${id}.xlsx`);
    await wb.xlsx.write(res);
  } catch (error) {
    next(error);
  }
}

const pdfFile = async function (req, res, next) {
  const paper = await Paper.findOne({ paperid: req.params.id })
  // const papertitle = paper.papertitle
  try {
    const stream = res.writeHead(200, {
      'Content-Type': 'application/pdf',
      'Content-Disposition': `attachment;filename=certificate.pdf`,
    });
    pdfService.buildPDF((chunk) => stream.write(chunk),() => stream.end(),paper);
  } catch (err) {
    return res.status(404).send({ message: err.message });
  }
}


function betweenRandomNumber(min, max) {
  return Math.floor(Math.random() * (max - min + 1) + min);
}

router.post("/", addConference);
router.get("/", getConfereces);
router.get("/:id", getConferece);
router.get("/papercon/:id", getPaperConfereces);
router.delete("/:id", deleteConference);
router.put("/:id", updateConference);
router.get("/excel/:id", exportExcel);
router.get("/pdf/:id", pdfFile)

module.exports = router;
