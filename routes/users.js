const express = require("express");
const { model } = require("mongoose");
const User = require("../model/User")
const router = express.Router();
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");

const addUser = async (req, res, next) => {
  const newUser = new User({
    userid: 1,
    conference: await Conference.findById(req.body.conference).exec(),
    name: req.body.name,
    email: req.body.email,
    usertype: req.body.usertype,
    description: req.body.description,
  });
  console.log("input Conference = " + req.body.Conference);
  try {
    await newUser.save();
    await Conference.findByIdAndUpdate(
      { _id: req.body.Conference },
      {
        $push: {
          users: await User.find({ name: req.body.name }).exec(),
        },
      }
    );
    res.status(201).json(newUser);
  } catch (err) {
    return res.status(500).send({
      message: err.message,
    });
  }
};
const getUsers = async (req, res, next) => {
  try {
    const users = await User.find({});
    res.status(200).json(users);
  } catch (err) {
    console.log("Error message : " + err);
    return res.status(500).send({
      message: err.message,
    });
  }
};
const getUser = async (req, res, next) => {
  const id = req.params.id;
  try {
    const user = await User.findById(id).exec();
    if (user === null) {
      return res.status(404).send({
        message: "User not found",
      });
    }
    res.json(user);
  } catch (err) {
    return res.status(404).send({
      message: err.message,
    });
  }
};
const deleteUser = async function (req, res, next) {
  const userName = req.params.id;
  try {
    await User.findByIdAndDelete(userName);
    return res.status(200).send();
  } catch (err) {
    return res.status(404).send({ message: err.message });
  }
};
const updateUser = async function (req, res, next) {
  const userName = req.params.id;
  try {
    const user = await User.findById(userName);
    console.log("User Name = " + userName);
    user.email = req.body.email;
    user.usertype = req.body.usertype;
    user.description = req.body.description;
    await user.save();
    res.status(200).json(user);
  } catch (err) {
    return res.status(404).send({ message: err.message });
  }
};

const login = async (req, res) => {
  // const { email, password } = req.body;
  try {
    const email = req.body.email
    const password = req.body.password
    console.log("email "+email+" password "+password)
    const user = await User.findOne({ email: email });
    console.log(user)
    if (user.email == email && user.password == password  ) {
      return res.status(200).send({ message: "OK 200" });
    } else {
      return res.status(404).send({ message: "BAD 404" });
    }
  } catch (error) {
    return res.status(404).send({ message: error });
  }
};

router.post("/", addUser);
router.get("/", getUsers);
router.get("/:id", getUser);
router.delete("/:id", deleteUser);
router.put("/:id", updateUser);
router.post("/login", login);

module.exports = router;
