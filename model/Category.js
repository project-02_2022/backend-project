const mongoose = require("mongoose");
const { Schema } = mongoose;
const CategorySchema = Schema({
    categoryid: Number,
    categoryname: String,
    categorydescription: String,
    papers:[]
})

module.exports = mongoose.model("Category", CategorySchema);
