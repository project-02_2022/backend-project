const mongoose = require("mongoose");
const { Schema } = mongoose;
const conferenceSchema = Schema({
  conferenceid: Number,
  conferencename: String,
  abbreviation: String,
  startdate: Date,
  enddate: Date,
  venus: String,
  type: String,
  organizer: String,
  // papers: [{ type: Schema.Types.ObjectId, ref: "Paper", default: [] }],
  papers: [],
});

module.exports = mongoose.model("conference", conferenceSchema);
