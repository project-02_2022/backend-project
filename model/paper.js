const mongoose = require("mongoose");
const { Schema } = mongoose;
const paperSchema = Schema({
  paperid: Number,
  conferenceid: Number,
  conference: Number,
  papertitle: String,
  sessions: String,
  paperstatus: String,
  detail: String,
  author: [],
  category: [],
});

module.exports = mongoose.model("Paper", paperSchema);
