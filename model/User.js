const mongoose = require('mongoose')
const { Schema } = mongoose
const UserSchema = Schema({
    name: String,
    email: String,
    password: String,
    usertype: String,
    description: String
})

module.exports = mongoose.model('User', UserSchema)
