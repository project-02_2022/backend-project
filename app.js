const express = require("express");
const cors = require("cors");
const path = require("path");
const cookieParser = require("cookie-parser");
const logger = require("morgan");

const mongoose = require("mongoose");
mongoose.set("strictQuery", false);
const papersRouter = require("./routes/paper");
const conferencesRouter = require("./routes/conference");
const usersRouter = require("./routes/users");
const categorysRouter = require("./routes/category");

// const dotenv = require("dotenv");
// get config vars
// dotenv.config();

mongoose.connect("mongodb://127.0.0.1:27017/IConference");

const app = express();

app.use(cors());
app.use(logger("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cookieParser());
// app.use(express.static(path.join(__dirname, "public")));

// app.use("/", indexRouter);
app.use("/papers", papersRouter);
app.use("/conferences", conferencesRouter);
app.use("/users", usersRouter);
app.use("/categorys", categorysRouter);

module.exports = app;
