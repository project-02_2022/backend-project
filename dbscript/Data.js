const mongoose = require("mongoose");
const Category = require("../model/Category");
mongoose.set("strictQuery", false);
// mongoose.connect("mongodb://localhost:27017/IConference");
mongoose.connect("mongodb://127.0.0.1:27017/IConference");
const Conference = require("../model/Conference");
const Paper = require("../model/Paper");
const User = require("../model/User");

async function clearDB() {
  await Conference.deleteMany({});
  await Paper.deleteMany({});
  await User.deleteMany({});
  await Category.deleteMany({});
}

async function main() {
  await clearDB();

  //Mooc Paper 1-5 conference 1
  const conference1 = new Conference({
    conferenceid: 111,
    conferencename: "กราฟฟิกศิลปะใหม่",
    abbreviation: "FA",
    startdate: new Date("2020-03-06"),
    enddate: new Date("2020-03-07"),
    venus: "Novotel Siam",
    type: "Onsite",
    organizer: "สถาบันวิจัยสุขภาพ",
  });
  const paper1 = new Paper({
    paperid: 194531,
    conference: conference1.conferenceid,
    papertitle: "Synthesized",
    sessions: "Poster",
    paperstatus: "disapproval",
    detail: "academic",
    author: ["pair", "kung"],
    category: [12345, 24521],
    categorys: []

  });
  const paper2 = new Paper({
    paperid: 202212,
    conference: conference1.conferenceid,
    papertitle: "Synthesized",
    sessions: "Regular",
    paperstatus: "disapproval",
    detail: "system",
    author: ["Harry", "potter"],
    category: [24521,12345],
    categorys: []
  });
  const paper3 = new Paper({
    paperid: 233469,
    conference: conference1.conferenceid,
    papertitle: "Investigate",
    sessions: "Special",
    paperstatus: "disapproval",
    detail: "academic",
    author: ["Harry", "ray"],
    category: [12345, 24521],
    categorys: []
  });
  const paper4 = new Paper({
    paperid: 983214,
    conference: conference1.conferenceid,
    papertitle: "Investigate",
    sessions: "Regular",
    paperstatus: "approval",
    detail: "system",
    author: ["Data", "Jack"],
    category: [24521,12345],
    categorys: []
  });
  const paper5 = new Paper({
    paperid: 780982,
    conference: conference1.conferenceid,
    papertitle: "ฤทธิ์ต้านแบคทีเรีย",
    sessions: "Regular",
    paperstatus: "approval",
    detail: "academic",
    author: ["Rayyyy", "Data"],
    category: [12345, 24521],
    categorys: []
  });
  const conference2 = new Conference({
    conferenceid: 222,
    conferencename: "ต้นไม้สร้างอาชีพ",
    abbreviation: "ARDA",
    startdate: new Date("2020-03-08"),
    enddate: new Date("2020-03-10"),
    venus: "Hua Hin Hotel",
    type: "Onsite",
    organizer: "สถาบันวิจัยศิลปะ",
  });
  //Mooc Paper 6-10 conference 2
  const paper6 = new Paper({
    paperid: 646609,
    conference: conference2.conferenceid,
    papertitle: "Synthesized ",
    sessions: "Poster",
    paperstatus: "disapproval",
    detail: "communication",
    author: ["Jack", "Rayray"],
    category: [24521,12345],
    categorys: []
  });
  const paper7 = new Paper({
    paperid: 651322,
    conference: conference2.conferenceid,
    papertitle: "อาหารท้องถิ่น",
    sessions: "Special",
    paperstatus: "disapproval",
    detail: "academic",
    author: ["Jack", "Leo"],
    category: [12345, 24521],
    categorys: []
  });
  const paper8 = new Paper({
    paperid: 455643,
    conference: conference2.conferenceid,
    papertitle: "basic health screening",
    sessions: "Special",
    paperstatus: "disapproval",
    detail: "communication",
    author: ["Ray", "Jack"],
    category: [24521,12345],
    categorys: []
  });
  const paper9 = new Paper({
    paperid: 168901,
    conference: conference2.conferenceid,
    papertitle: "Investigate",
    sessions: "Regular",
    paperstatus: "approval",
    detail: "academic",
    author: ["Jack", "Ray"],
    category: [12345, 24521],
    categorys: []
  });
  const paper10 = new Paper({
    paperid: 150932,
    conference: conference2.conferenceid,
    papertitle: "ฤทธิ์ต้านแบคทีเรีย",
    sessions: "Regular",
    paperstatus: "approval",
    detail: "system",
    author: ["Harry", "Leo"],
    category: [24521,12345],
    categorys: []
  });

  //Paper not conferences
  const paper11 = new Paper({
    paperid: 322211,
    conference: 0,
    papertitle: "Synthesized",
    sessions: "Poster",
    paperstatus: "disapproval",
    detail: "academic",
    author: ["Leo", "Data"],
    category: [12345,24521],
    categorys: []
  });
  const paper12 = new Paper({
    paperid: 126709,
    conference: 0,
    papertitle: "อาหารท้องถิ่น",
    sessions: "Poster",
    paperstatus: "disapproval",
    detail: "communication",
    author: ["Data", "Leo"],
    category: [24521,12345],
    categorys: []
  });
  const paper13 = new Paper({
    paperid: 112098,
    conference: 0,
    papertitle: "basic health screening",
    sessions: "Special",
    paperstatus: "disapproval",
    detail: "communication",
    author: ["Rayray", "Jack"],
    category: [12345,24521],
    categorys: [],
  
  });

  //Mooc User
  const user1 = new User({
    name: "Yaya",
    email: "Yaya01@gmail.com",
    password: "Yaya",
    usertype: "admin",
    description: "test",
  });
  const user2 = new User({
    name: "Kate",
    email: "Kate@gmail.com",
    password: "user2",
    usertype: "finance",
    description: "test",
  });

  const useradmin = new User({
    name: "admin",
    email: "admin",
    password: "admin",
    usertype: "admin",
    description: "test",
  });
  const categoty1 = new Category({
    categoryid: 12345,
    categoryname : "Computer Networks",
    categorydescription : "ประวัติศาสตร์",
    papers:[111111,452111],
  });
  const categoty2 = new Category({
    categoryid: 24521,
    categoryname : "Software Testing",
    categorydescription : "วินิจฉัย",
    papers:[111111,555555]
  });


  // //push papers to conference 1
  conference1.papers.push(paper1.paperid);
  conference1.papers.push(paper2.paperid);
  conference1.papers.push(paper3.paperid);
  conference1.papers.push(paper4.paperid);
  conference1.papers.push(paper5.paperid);

  //push papers to conference 2
  conference2.papers.push(paper6.paperid);
  conference2.papers.push(paper7.paperid);
  conference2.papers.push(paper8.paperid);
  conference2.papers.push(paper9.paperid);
  conference2.papers.push(paper10.paperid);

  // //Save Conference 1 and paper 1-5
  await conference1.save();
  await paper1.save();
  await paper2.save();
  await paper3.save();
  await paper4.save();
  await paper5.save();

  //Save Conference 2 and paper 6-10
  await conference2.save();
  await paper6.save();
  await paper7.save();
  await paper8.save();
  await paper9.save();
  await paper10.save();

  //Save Paper not conference
  await paper11.save();
  await paper12.save();
  await paper13.save();

  //Save Data User
  await user1.save();
  await user2.save();
  await useradmin.save();

  //Save category1-2
  await categoty1.save();
  await categoty2.save();
}

main().then(function () {
  console.log("Insert Data Finish... \n => Ctrl + c Exit");
});
