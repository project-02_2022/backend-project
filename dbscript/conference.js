const mongoose = require("mongoose");
mongoose.set("strictQuery", false);
const Conference = require("../model/Conference");
mongoose.connect("mongodb://localhost:27017/IConference");
async function clearConference() {
  await Conference.deleteMany({});
}

async function main() {
  await clearConference();
  const Conference1 = new Conference({
    conferenceid: 1,
    conference: "ความก้าวหน้าไอที",
    abbreviation: "GISTDA",
    datestart: new Date("2020-03-03 08:00 "),
    dateend: new Date("2020-03-03 08:00 "),
    venus: "Puket Hotel",
    type: "Onsite",
    organizer: "สำนักงานส่งเสริมไอที",
  });
  Conference1.save();
}

main().then(function () {
  console.log("Finish Conference");
});
