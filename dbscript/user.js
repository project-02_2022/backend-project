const mongoose = require("mongoose");
mongoose.set("strictQuery", false);
const User = require("../model/User");
mongoose.connect("mongodb://localhost:27017/IConference");
async function clearUser() {
  await User.deleteMany({});
}

async function main() {
  await clearUser();
  const user1 = new User({
    name: "Yaya",
    email: "Yaya@gmail.com",
    password: "Yaya",
    usertype: "admin",
    description: "test",
  });
  user1.save();
  const user2 = new User({
    name: "Kate",
    email: "Kate@gmail.com",
    password: "Kate",
    usertype: "finance",
    description: "test",
  });
  user2.save();
}

main().then(function () {
  console.log("Finish User");
});
