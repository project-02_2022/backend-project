const mongoose = require("mongoose");
const Category = require("../model/Category");
mongoose.set('strictQuery', false);
const Paper = require('../model/Paper')
// mongoose.connect('mongodb://localhost:27017/IConference')
mongoose.connect('mongodb://127.0.0.1:27017/IConference');

async function clearPaper () {
    await Paper.deleteMany({})
}

async function main () {
    await clearPaper()
    const paper1 = new Paper({
        paperid:536734,
        conferenceid:1,
        papertitle:"Investigate",
        sessions:"Regular",
        paperstatus:"approval",
        detail:"Webside",
        category: [12345, 24521]
    })
    paper1.save()

}

main().then(function(){
    console.log('Finish Paper')
})