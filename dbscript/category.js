const mongoose = require("mongoose");
mongoose.set("strictQuery", false);
const Category = require("../model/Category");
mongoose.connect("mongodb://127.0.0.1:27017/IConference");
async function clearCategory(){
    await Category.deleteMany({});
}

async function main() {
    await clearCategory();

    const ComputerNetworks = new Category({
        categoryid: 12345,
        categoryname : "Computer Networks",
        categorydescription : "ประวัติศาสตร์",
        papers:[111111]
    })
    ComputerNetworks.save();

    const SoftwareTesting = new Category({
        categoryid: 24521,
        categoryname : "Software Testing",
        categorydescription : "วินิจฉัย",
        papers:[111111,555555]

    })
    SoftwareTesting.save();
}

main().then(function () {
    console.log("Finish Category");
  });